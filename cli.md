# Using the command line

While graphical user interface (GUI) tools exist for practically every task the *average* modern computer user needs, the same is not true for today's astronomers.  Many programs and utilities used daily by professional astronomers can only be accessed through the command line interface (CLI).  Moreover, many common cumbersome tasks conducted in GUIs (such as renaming many files at a time, searching through many files for specific text, etc.) can be concisely and efficiently completed using a CLI.

The first task then is to access the CLI for your computer via a terminal emulator.

If your computer is running macOS, open the Applications folder, then enter the Utilities folder, and finally select the Terminal application.  Alternatively, click on the magnifying glass icon on the upper right task bar of your screen and type in `Terminal`.  Doing so will open a window that looks something like this:

![](assets/terminal.png)

If you are using a PC, open the start menu and search for `PowerShell`.  Launching it should yield a window that looks something like this:

![](assets/powershell.png)

If you have a computer running a Linux distribution, you should already know how to launch a terminal emulator.  If not, why are you using Linux?

Now that you have a terminal emulator open, it's time to run a couple basic commands!  To see the *path* (i.e. location) of the current directory your CLI has accessed, type in

~~~bash
pwd
~~~

(which stands for **p**rint **w**orking **d**irecory) and then press enter to execute the command.

To **c**hange the **d**irectory you're in, type

~~~bash
cd <path>
~~~

where `<path>` is replaced with the path to the desired directory.  For example

~~~bash
cd Documents
~~~

***Protip:*** *in macOS/Windows, if you do not know the path to the directory you would like to open, but can get to the folder via Finder/File Explorer, simply click and drag the folder icon into your terminal and it will paste the path in there for you.*

For now, the above is sufficient to get you through the remainder of the tutorial.  As you progress through this course (and beyond), you will pick up various additional commands and terminal tricks.  Keep in mind though that the CLI commands/utilities used in macOS to conduct a specific task are in general ***not*** the same as in Windows (although most common macOS commands are the same as in other Unix-like operating systems, such as Linux).

If you are ever curious about how to use a particular command, or what all its options are, you can view its **man**ual page with

~~~bash
man <command>
~~~
