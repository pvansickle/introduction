{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"alert alert-info\" role=\"alert\"><b>Prerequisites:</b> <a href=\"https://www.continuum.io/downloads\">Anaconda (Python 3.x)</a> and <a href=\"https://git-scm.com/\">Git</a> need to be installed on your computer. The astronomy department offers desktops in Angell Hall (5180A and 5190) and on the 4th Floor of West Hall (the undergraduate \"lab\") with the prerequisite software installed.</div>\n",
    "\n",
    "Before proceeding with this notebook, make sure you have read through the [Git introduction document](readme.md) and set up your BitBucket account."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Jupyter notebook\n",
    "\n",
    "A Jupyter notebook is an interactive environment that allows you to write and execute code snippets, as well as write explanatory text (including equations and visualizations) to augment your code. Formerly known as iPython notebooks, Jupyter notebooks retain the `.ipynb` file extension (although Jupyter notebooks can be used with programming languages besides python as well).  A breif introduction to the user interface can be found [here](http://nbviewer.jupyter.org/github/ipython/ipython-in-depth/blob/master/examples/Notebook/Notebook%20Basics.ipynb).\n",
    "\n",
    "To launch Jupyter, first open a terminal emulator.  Navigate to the directory containing the Jupyter notebook you want to work on and then enter the command\n",
    "\n",
    "    jupyter notebook\n",
    "\n",
    "This will launch a Jupyter session in your web browser.  From here, you can select the notebook(s) you want to interact with.  To execute a cell, press <kbd>Ctrl</kbd>-<kbd>Enter<"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This notebook is designed for astronomers or future astronomers with little or no programming skills.  Some prior knowledge about the command-line and using a termimal to run code and manage programs and data is very helpful.\n",
    "\n",
    "As you work through this tutorial, you should try and break it. After you download it, the file belongs to you. Please mess with it. Enter the code and markdown blocks. Change the numbers. Type <kbd>shift</kbd>+<kbd>tab</kbd> (twice) on code commands/functions that you don't understand. *Learn by doing.*"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Here is an **image** of M42 I found on the internet (*credit [NOAO](https://www.noao.edu/image_gallery/images/d3/02677a.jpg)*):"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "from IPython.display import Image\n",
    "Image(filename='02677a.jpg')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "print(\"Behold, the Orion Nebula.\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In the cell above, highlight the word **`print`** and type <kbd>shift</kbd>+<kbd>tab</kbd> simultaneously. Do it again. What do you see?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As you make a Jupyter notebook, you can save it as a Jupyter notebook (i.e., with an `.ipynb` extension). What does a Jupyter file actually look like?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "from IPython.display import Image\n",
    "Image(filename='raw.png')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## NumPy examples\n",
    "\n",
    "[NumPy](http://www.numpy.org) is the main Python package for working with N-dimensional arrays. Any list of numbers can be recast as a NumPy array:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "x = np.array([1, 5, 3, 4, 2])\n",
    "x"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Arrays have a number of useful methods associated with them:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "print(x.min(), x.max(), x.sum(), x.argmin(), x.argmax())"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "and NumPy functions can act on arrays in an elementwise fashion:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "np.sin(x * np.pi / 180.)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Ranges of values are easily produced:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "np.arange(1, 10, 0.5)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "np.linspace(1, 10, 5)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "np.logspace(1, 3, 5)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Random numbers are also easily generated in the half-open interval [0, 1):"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "np.random.random(10)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "or from one of the large number of statistical distributions provided:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "np.random.normal(loc = 2.5, scale = 5, size = 10)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Another useful method is the **`where`** function for identifying elements that satisfy a particular condition:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "x = np.random.normal(size = 300)\n",
    "np.where(x > 2.)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Of course, all of these work equally well with multidimensional arrays."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "x = np.array([[1, 2, 3, 4, 5], [6, 7, 8, 9, 10]])\n",
    "np.sin(x)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## <font color = \"crimson\">Exercise 1:</font> <button type=\"button\" class=\"btn btn-xs\">10 pts</button>\n",
    "\n",
    "Create two 4x4 matrices and use Python and Numpy to perform a matrix multiplication. Confirm your result and show your work."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "#Your code here"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Looping\n",
    "\n",
    "In the following exercises, we will implement loops.\n",
    "\n",
    "### Sources of Error\n",
    "\n",
    "With any algorithm, there are always two sources of error: *roundoff error* and *truncation error*. These errors can be exacerbated when looping.\n",
    "\n",
    "#### Example: Roundoff error \n",
    "\n",
    "This arises from the error inherent in representing a floating point number\n",
    "with a finite number of bits in the computer memory.  Look at the example below that demonstrates roundoff error.  Find $\\epsilon$ that satisfies the equation $1 + \\epsilon = 1$. You would think (in the analytical world) that it simply means $\\epsilon = 0$.  However, in the numerical world we find that this equation is satisfied when the value of $\\epsilon > 0$!  What is that strange value?  It is the limit of machine precision.  \n",
    "\n",
    "Let's start with $\\epsilon = 1$ and iterate, halving $\\epsilon$ each time until in the computer representation $1 + \\epsilon = 1$.  Try it (<kbd>shift</kbd>+<kbd>enter</kbd> to execute the cell below)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "eps = 1.0\n",
    "while 1.0 + eps != 1.0:\n",
    "    eps = eps/2.\n",
    "print(\"eps = {}\".format(eps))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Why is `eps` not equal to zero? Should it be?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now let's consider *truncation error*. Truncation error is a feature of an algorithm: we approximate an equation or function by expanding about some small quantity. By throwing away higher-order terms, we are truncating that expression and introducing an error in the representation. If the quantity we expand about is small, then the error is\n",
    "small. But it is not always the case and we should be vareful.  \n",
    "\n",
    "Look at the example below, to approximate the cosine function by the Taylor series:\n",
    "\n",
    "$$\\cos(x) = \\sum_{k=0}^\\infty (-1)^k {x^{2k} \\over (2k)!}$$\n",
    "\n",
    "#### Example: Truncation Error\n",
    "\n",
    "First, we define a function that calculates the first ten terms of the expansion.  Look at the Python syntax.  It is quite readable.  The thing to notice is the indentation to the right after the **`def`** line.  Also, the first line imports a standard **`math`** library, which contains standard functions such as the factorial."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "import math\n",
    "def fn(x):\n",
    "    f = 1.0\n",
    "    for k in range(1, 10):\n",
    "        f += (-1)**k*x**(2*k)/math.factorial(2.*k)\n",
    "    return f"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now we can evaluate it at several values of x.  Let's choose $\\pi/2$, $5\\pi/2$, and $9\\pi/2$, *for all of which the analytical answer is 0*.  Try it."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "pi = math.pi\n",
    "print(fn(pi/2.), fn(pi*5./2.), fn(pi*9./2.))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The result may seem strange at first, but it should not surprise us.  All three arguments are greater than unity, and therefore convergence of the series is slow.  Especially, for $x = 9\\pi/2 \\approx 14$.\n",
    "\n",
    "This is also a good place to think about the efficiency of our calculation.  Each new term in the series is based on the previous term, times an additional factor.  Since we know this property before making the calculation, we can utilize it to reduce the number of multiplications and speed up the code. Look at the function below.  We can use an internal Python routine to time our calculations and compare the speed of evaluation of the two functions."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "print(fn(pi*9./2.))\n",
    "%timeit -n 5000 fn(pi*9./2.)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## <font color = \"crimson\">Exercise 2:</font> <button type=\"button\" class=\"btn btn-xs\">10 pts</button>\n",
    "\n",
    "Write code to find $k$ such that **`fn(x)`** converges (i.e., that by increasing $k$, the result no longer changes)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": true
   },
   "source": [
    "## Reading in ascii data from a file\n",
    "\n",
    "Data can also be automatically loaded from a file into a Numpy array via the **`loadtxt`** or **`genfromtxt`** methods. Make sure you have the datafile `andreon_tab1.csv`. in your working directory, or specify your path, or append a path to the system. *Note that if you reset your path, you must reload NumPy in order for loadtxt to see the revision.*"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true,
    "scrolled": false
   },
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "dt =  [('m200',float),('m200e',float),('obstot1',int),('obsbkg1',int), \n",
    "    ('c',float)]\n",
    "data1 = np.loadtxt('andreon_tab1.csv', dtype=dt, skiprows=(1), \n",
    "                   usecols=(11,12,1,2,3), delimiter=',')\n",
    "from subprocess import check_output\n",
    "from pprint import pprint\n",
    "pprint(check_output(['more', 'andreon_tab1.csv']))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "print(data1['m200'])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Matplotlib visualization exercises"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "%matplotlib inline\n",
    "%config InlineBackend.figure_format = 'svg'\n",
    "import matplotlib.pyplot as plt\n",
    "from matplotlib import rcParams\n",
    "rcParams['figure.figsize'] = (7.5, 5.0) # Make plots bigger\n",
    "x = np.arange(0,99,dtype=float)\n",
    "y = np.random.normal(0,10,(99,))\n",
    "plt.plot(x,y)\n",
    "plt.ylabel('Y axis')\n",
    "plt.xlabel('X axis')\n",
    "plt.title('Myplot')\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Side by side panels:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "f, ((ax1, ax2), (ax3, ax4)) = plt.subplots(2, 2, sharex='col', sharey='row')\n",
    "ax1.plot(x, y)\n",
    "ax1.set_title('Sharing x per column, y per row')\n",
    "ax2.scatter(x, y)\n",
    "ax3.scatter(x, 2 * y ** 2 - 1, color='r')\n",
    "ax4.plot(x, 2 * y ** 2 - 1, color='r')\n",
    "plt.show() # OR plt.savefig('sine_function_legend.png')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Including errors:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "x, y = np.random.random([2, 100])\n",
    "xerr = np.random.normal(0., 0.05, 100)\n",
    "yerr = np.random.normal(0., 0.05, 100)\n",
    "plt.errorbar(x, y, xerr, yerr, fmt = '.k', \n",
    "                ecolor = 'gray', lw = 1, ms = 4)\n",
    "plt.xlabel('Abscissa')\n",
    "plt.ylabel('Ordinate')\n",
    "plt.xlim(0.0, 1.0)\n",
    "plt.ylim(0.0, 1.0)\n",
    "plt.grid(True)\n",
    "plt.title('A plot with errorbars')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "3D visualizations:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "from mpl_toolkits.mplot3d import Axes3D\n",
    "from matplotlib import cm\n",
    "\n",
    "def fun(x, y):\n",
    "     return np.sqrt(pow(x,2) + pow(y,2))\n",
    "\n",
    "x = y = np.arange(-99,99,dtype=float)\n",
    "X, Y = np.meshgrid(x, y)\n",
    "zs = np.array([fun(x,y) for x,y in zip(np.ravel(X), np.ravel(Y))])\n",
    "Z = zs.reshape(X.shape)\n",
    "fig = plt.figure()\n",
    "ax = fig.gca(projection='3d')\n",
    "ax.plot_surface(X, Y, Z, rstride=8, cstride=8, alpha=0.3, \n",
    "                label = 'Euclidean Distance')\n",
    "cset = ax.contour(X, Y, Z, zdir='z', offset=0, cmap=cm.coolwarm)\n",
    "cset = ax.contour(X, Y, Z, zdir='x', offset=-100, cmap=cm.coolwarm)\n",
    "cset = ax.contour(X, Y, Z, zdir='y', offset=100, cmap=cm.coolwarm)\n",
    "ax.set_xlabel('X')\n",
    "ax.set_xlim(-100, 100)\n",
    "ax.set_ylabel('Y')\n",
    "ax.set_ylim(-100, 100)\n",
    "ax.set_zlabel('Z')\n",
    "ax.set_zlim(0, 100)\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's make a simple histogram. What should these data look like?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true,
    "scrolled": false
   },
   "outputs": [],
   "source": [
    "x = np.concatenate([np.random.normal(0., 1., 100), \n",
    "                    np.random.normal(2, 0.5, 50)])\n",
    "plt.hist(x)\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Scientific Python (SciPy) examples\n",
    "\n",
    "Let's perform a definite integral:\n",
    "\n",
    "$$\\int_{1000}^{\\infty}\\frac{dx}{100x^3}$$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "import scipy as sp\n",
    "from scipy.integrate import quad\n",
    "\n",
    "def f(x):\n",
    "    return 0.01*x**-3\n",
    "\n",
    "integral, error = quad(f, 1000, sp.inf, epsrel = 1e-6,epsabs = 0)\n",
    "print(integral, error)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "How do we fit a curve to some data? Let's read in a data file and fit a Gaussian to its contents:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "dt = [('x',float), ('y',float)]\n",
    "data = np.loadtxt('data1.txt', dtype=dt, skiprows=(1))      \n",
    "x = data['x']\n",
    "y = data['y']\n",
    "index_array = np.where((x >= -10) & (x <= 10))\n",
    "x0 = x[index_array]\n",
    "y0 = y[index_array]    \n",
    "\n",
    "from scipy.optimize import curve_fit\n",
    "def gauss(x, *p):                                                             \n",
    "        A, mu, sigma = p\n",
    "        return A*np.exp(-(x-mu)**2/(2.*sigma**2))\n",
    "p0 = [1., 0., 1.]\n",
    "coeff, var_matrix = curve_fit(gauss, x0, y0, p0=p0)\n",
    "hist_fit = gauss(x0, *coeff)\n",
    "plt.plot(x0,y0,'o')\n",
    "plt.plot(x,y,'.')\n",
    "plt.plot(x0,hist_fit,label='Fitted Data')\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Reading and Plotting Astronomical Image Data\n",
    "\n",
    "Make sure you have the file `phiand.fits` in your working directory."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "from astropy.io import fits\n",
    "image = fits.info('phiand.fits')\n",
    "image\n",
    "data = fits.getdata('phiand.fits')\n",
    "plt.plot(data)\n",
    "plt.show() # what is this?\n",
    "plt.imshow(data)\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "What do the above two panels represent?\n",
    "\n",
    "Now, let's smooth the second image with a Gaussian kernal:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "from scipy import ndimage\n",
    "blurred_data = ndimage.gaussian_filter(data,sigma=3)\n",
    "plt.imshow(blurred_data)\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Do you see one or two objects (or more?) in the above image?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## <font color = \"crimson\">Exercise 3:</font>  <button type=\"button\" class=\"btn btn-xs\">20 pts</button>\n",
    " \n",
    "Read in the image called `andromeda_rot.fits`. Make a side-by-side 2-panel figure. The left panel should show the 2D representation of the pixels along with isocontours. The right panel should show a 2D Gaussian fit to the pixel intensities (hint: go to the SciPy cookbook and search for 2D Gaussian). Make the labels be in arcseconds (not pixels). In order to switch from pixels to arcseonds, you will need to extract the image tick values and use the known fact that every pixel is 1.7 arcseconds (the resolution of the instrument).\n",
    "\n",
    "**Extra Credit**:  <button type=\"button\" class=\"btn btn-xs\">5 pts</button> Re-do the Gaussian fit, but now solve for the rotation angle of the 2D Gaussian (i.e., the covariance)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## <font color = \"crimson\" >Exercise 4:</font> <button type=\"button\" class=\"btn btn-xs\">10 pts</button>\n",
    "\n",
    "Create a BitBucket repository (called Introduction).  Ininitialize a new Git directory on your computer.  Within it, create a new Jupyter notebook that contains the above three exercises. Commit and push your notebook into your BitBucket repository.  *Make sure you save your notebook before commiting with <kbd>⌘</kbd>-<kbd>S</kbd> / <kbd>Ctrl</kbd>-<kbd>S</kbd> in Mac/Windows, respectively.* Give the instructors access to this repo (usernames: **jdmonnier**, **mm_cra**, and **bensetterholm**). The instructors will check out your code and run it."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "---\n",
    "\n",
    "Congratulations, you have come to the end of the lab!  Please submit your lab report to canvas as an html file.\n",
    "\n",
    "In Jupyter, select: File &blacktriangleright; Download as &blacktriangleright; HTML (.html)"
   ]
  }
 ],
 "metadata": {
  "anaconda-cloud": {},
  "kernelspec": {
   "display_name": "Python [Root]",
   "language": "python",
   "name": "Python [Root]"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 2
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython2",
   "version": "2.7.12"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 1
}
